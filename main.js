const puppeteer = require('puppeteer');
const { writeFileSync, appendFile } = require("fs");

const RESULT_FILE = 'company_details.csv';
const HREF_SELECTOR = '.GIY1LSJIXC a';
const DETAIL_VALUE_SELECTOR = '#commentOnLink a';
const DETAIL_KEY_SELECTOR = 'h1.GIY1LSJBID';
const MAX_PAGES = 550;

writeFileSync(RESULT_FILE, 'key%%value');

(async () => {
  const browser = await puppeteer.launch({
  });
  const outerpage = await browser.newPage();
  
  // Loop thru all pages
  for (let i = 0; i <= MAX_PAGES; i += 50) {
    await outerpage.goto(generateUrl(i));
    
    await outerpage.waitFor('body');
    await outerpage.waitForFunction(
      'document.querySelectorAll("' + HREF_SELECTOR +'").length === 50');
    
    const allLinks = await outerpage.evaluate((localHrefSelector) => {
      return Array.from(document.querySelectorAll(localHrefSelector)).map(x => x.href);
    }, HREF_SELECTOR);
    
    // TODO: change it back to allLinks.length.
    // Loop thru each href link in one page (50 per page).
    for (let j = 0; j < allLinks.length; j++) {
        const innerpage  = await browser.newPage();
        innerpage.goto(allLinks[j]);
        
        // Wait for all required DOMs to be loaded.
        await innerpage.waitFor('body');
        await innerpage.waitFor(DETAIL_KEY_SELECTOR);
        await innerpage.waitFor(DETAIL_VALUE_SELECTOR);
        
        const detailKey =
            await innerpage.evaluate((localKeySelector) => {
          return document.querySelector(localKeySelector).innerText;
        }, DETAIL_KEY_SELECTOR);
        const detailValue =
            await innerpage.evaluate((localValueSelector) => {
          return document.querySelector(localValueSelector).innerText;
        }, DETAIL_VALUE_SELECTOR);
        // await innerpage.evaluate(() => {debugger;});
        
        const toWriteString = detailKey + '%%' + detailValue + '\n';
        appendFile(RESULT_FILE, toWriteString, err => {
          if (err) throw err;
        });
        console.log("done for innerpage: " + j);
        await innerpage.close();
    }
    console.log("done for outerpage: " + i);
  }
  await browser.close();
})();

function generateUrl(pageIndex) {
  return 'https://www.regulations.gov/searchResults?rpp=50&po=' + pageIndex + '&s=bis-2018-0006%2Bdecision%2Bmemo&dct=FR%2BPR%2BN%2BO%2BSR%2BPS';
}
